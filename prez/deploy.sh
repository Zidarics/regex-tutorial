#!/bin/sh

SUBJECT=regex
REMOTE_HOST=zamek
REMOTE_DIR=html/$SUBJECT

find . -name "*.svg" -exec perl -p -i -e 's/xlink:href=\"\/home\/zamek\/oktat\/$SUBJECT\//xlink:href="/' {} \;

tar -cvzf - * | ssh $REMOTE_HOST "cd $REMOTE_DIR; rm -rf *; tar -xvzf -"

