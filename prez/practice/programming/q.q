package hu.innobyte.gondosora.processor.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.innobyte.gondosora.common.configuration.AppConfig;
import hu.innobyte.gondosora.common.domain.dto.SkillDTO;
import hu.innobyte.gondosora.common.domain.jms.cbs.UpdateAgentStateData;
import hu.innobyte.gondosora.common.domain.jpa.agentcache.AgentStatus;
import hu.innobyte.gondosora.common.repository.PendingTicketRepository;
import hu.innobyte.gondosora.processor.service.PendingTicketService;
import jakarta.annotation.PostConstruct;
import jakarta.jms.Message;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.function.Failable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


@Slf4j
@Component
public class CmsRefreshAgentState {

    /**
     * activates some developer codes if true
     */
    private static final boolean TESTING=false;

    private final PendingTicketService pendingTicketService;

    private final Map<String, DefaultMessageListenerContainer> consumers;

    private final AppConfig appConfig;

    private final PendingTicketRepository pendingTicketRepository;

    private final ObjectMapper objectMapper;

    private final JdbcTemplate jdbcTemplate;

    private final JmsTemplate cbsTemplate;

    private final AgentStateChangeEventPublisher eventPublisher;

    private final AppStatistics appStatistics;

    private Instant lastReady;

    @Autowired
    public CmsRefreshAgentState(PendingTicketService pendingTicketService,
                                @Qualifier("goraListenerContainers")
                                Map<String, DefaultMessageListenerContainer> consumers,
                                @Qualifier("cbsTemplate") JmsTemplate cbsTemplate,
                                PendingTicketRepository pendingTicketRepository,
                                JdbcTemplate jdbcTemplate,
                                ObjectMapper objectMapper,
                                AppConfig appConfig,
                                AgentStateChangeEventPublisher eventPublisher,
                                AppStatistics appStatistics) {
        this.pendingTicketService = pendingTicketService;
        this.consumers = consumers;
        this.appConfig = appConfig;
        this.pendingTicketRepository = pendingTicketRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
        this.cbsTemplate = cbsTemplate;
        this.eventPublisher=eventPublisher;
        this.appStatistics=appStatistics;
        setLastReady();
    }

    @PostConstruct
    private void postCOnstruct() {
	try {
	    sendTestMessage();
	}
	catch(Exception e) {
	    log.error("TestMessage error:", e);
	}
    }


    /**
     * Don't use  only for developers
     * @throws JsonProcessingException
     */
    @Deprecated
    private void sendTestMessage() throws JsonProcessingException {
        if (!TESTING)
            return;
        List<String> skills=new ArrayList<>();
        skills.add("VO_Diszpecser_L2");
        skills.add("VO_Diszpecser_L1");
        enableConsumerOptional(skills);

        this.appConfig.getGoraConfig()
                .getConsumers()
                .forEach(c->setConsumer(c.getTopic(), 1));


        String agent="DDJSZ\\kerekes.peter";
        log.warn("CmsRefreshAgentState.TEST is enabled, send a fake message");

        this.jdbcTemplate.update(String.format("Update agent_state set state='R', state_reason='Ready' where agent_common_id='%s'", agent));

        List<SkillDTO> skillsDTOS = new ArrayList<>();
        skillsDTOS.add(SkillDTO.builder()
                .name("VO_Diszpecser_L1")
                .numberOfNotReady(1)
                .numberOfReady(3)
                .level(1)
                .build());
        skillsDTOS.add(SkillDTO.builder()
                .name("VO_Diszpecser_L2")
                .numberOfActive(1)
                .numberOfNotReady(1)
                .numberOfReady(3)
                .level(1)
                .build());

        String cbsMessage = this.objectMapper.writeValueAsString(UpdateAgentStateData.builder()
                .agentId("5eec82a3-ee66-4e7c-8293-0fd8dd20d144")
                .agentCommonId(agent)
                .state(AgentStatus.READY)
                .skills(skillsDTOS)
                .componentName("Gora")
                .stateReason("READY")
                .stateChangeTime(LocalDateTime.now())
                .supervisorId("sv")
                .supervisorCommonId("svc")
                .build());

        cbsTemplate.convertAndSend(appConfig.getCbsConfig().getInputQueues().getRefreshAgentState(), cbsMessage);
    }
}

