#!/usr/bin/awk -f

function trim(instr) {
    gsub("^[ ]+","",instr);
    gsub("[ ]+$","", instr);
    return instr;
}

function strnull(instr) {
   if (instr ~ /^$/)
     return "null";
   
   return "'" instr "'";
}

function normalize(instr) {
  instr = trim(instr);
  gsub("\"","", instr);
  return instr;
}

function createTables() {
  print "create table nation(id int not null auto_increment primary key,";
  print "\tcode varchar(10), name varchar(30) "
  print ");";
  print "";
  
  print "create table cargroup(id int not null auto_increment primary key,";
  print "\tcode varchar(10), name varchar(30) "
  print ");";
  print "";
  
  print "create table carclass(id int not null auto_increment primary key,";
  print "\tgroup_id int, "
  print "\tcode varchar(10), name varchar(30), ";
  print "\tforeign key (group_id references cargroup(id) on delete cascade) ";
  print ");";
  print "";
  
  print "create table participant(id int not null auto_increment primary key,";
  print "\tname varchar(50) "
  print ");";
  print "";
  
  print "create table racer(id int not null auto_increment primary key,";
  print "\tstartnum int, ";
  print "\tpilot varchar(30), ";
  print "\tcopilot varchar(30), ";
  print "\tcar varchar(30), ";
  print "\tasn varchar(10), ";
  print "\tgroup_id int, ";
  print "\tcarclass_id, int";
  print "\tforeign key (part_id references participant(id) on delete cascade), ";
  print "\tforeign key (carclass_id references carclass(id) on delete cascade)"; 
  print " );";
  print "";
    
  print "insert into nation (code, name) values ('H', 'Magyarország');";
  print "insert into nation (code, name) values ('HR', 'Horvátország');";
  print "insert into nation (code, name) values ('A', 'Ausztria');";
  print "insert into nation (code, name) values ('SK', 'Szlovákia');";
  print "insert into nation (code, name) values ('RO', 'Románia');";
  print "insert into nation (code, name) values ('SLO', 'Szlovénia');";
  
  nations["H"]=0;
  nations["HR"]=0;
  nations["A"]=0;
  nations["SK"]=0;
  nations["RO"]=0;
  nations["SLO"]=0;
  
  print "insert into cargroup (code, name) values ('A','A csoport');";
  print "insert into cargroup (code, name) values ('N','N csoport');";
  print "insert into cargroup (code, name) values ('H','H csoport');";
  print "insert into cargroup (code, name) values ('WRC','WRC csoport');";
  print "insert into cargroup (code, name) values ('IRC','IRC csoport');";
  
  cargroups[A]=0;
  cargroups[N]=0;
  cargroups[H]=0;
  cargroups[WRC]=0;
  cargroups[IRC]=0;
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'N1', 'N1 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='N';";
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'N2', 'N2 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='N';";

  print "insert into cargroup (code, name, group_id) ";
  print "select 'N3', 'N3 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='N';";
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'N4', 'N4 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='N';";
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'A5', 'A5 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='A';";

  print "insert into cargroup (code, name, group_id) ";
  print "select 'A6', 'A6 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='A';";

  print "insert into cargroup (code, name, group_id) ";
  print "select 'A7', 'A7 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='A';";
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'A8', 'A8 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='A';";
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'H9', 'H9 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='H';";
  
  print "insert into cargroup (code, name, group_id) ";
  print "select 'H10', 'H10 géposztály', select cargroup.id from cargroup where ";
  print "cargroup.name='H';";
  
  carclasses["N1"]=0;
  carclasses["N2"]=0;
  carclasses["N3"]=0;
  carclasses["N4"]=0;
  carclasses["A5"]=0;
  carclasses["A6"]=0;
  carclasses["A7"]=0;
  carclasses["A8"]=0;
  carclasses["H9"]=0;
  carclasses["H10"]=0;
  
}

BEGIN { 
  FS="\t+"
  createTables();
}

NR>=6 {
  split($1, a, ".");
  startnum=a[1];
  
  if (int(startnum)==0 || $2 ~ /^[ \t]*$/ )
    next;
  
  racer=a[2]a[3];
  split(racer,b,"-");
  pilot=normalize(b[1]);
  copilot=normalize(b[2]);
  split($5, a, " ");
  asn = trim(a[2]);
  s=a[1];
  gsub("[0-9]+$","",s);
  cargroup = trim(s);
  s=a[1];
  gsub("^[A-Z]", "", s);
  carclass = trim(s);
  participant = normalize($3);
  
  printf "\ninsert into participant(name) values ('%s')\n", participant;
  print "insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)";
  printf "select %s, %s, %s, %s, %s, \nparticipant.id, carclass.id \nfrom participant, carclass",
	  startnum, strnull(pilot), strnull(copilot), strnull(normalize($4)), strnull(asn);
  printf "\nwhere participant.name='%s' and carclass.code='%s';\n", participant, carclass;   
  
  ++nation[trim($2)];
  ++racercount;
  ++cargroupcounts[cargroup];
  ++carclasscounts[carclass];
  ++asncounts[asn];
  
#  printf "sn:%s, pilot:%s, copilot:%s participant:%s, cargroup:%s, carclass:%s, asn:%s\n",
#	startnum, pilot, copilot, participant, cargroup, carclass, asn;
}


END{
   printf "All racer counts:%d\n", racercount;

   sum=0;
   printf "Nations:\n";
   for (i in nation) {
     sum += nation[i];
     printf "%s: %d\n", i, nation[i];
   }
   
   diff=int(racercount)-int(sum);
   if (diff==0)
      print "Nation check is OK";
   else
     printf "%d nations missed\n", diff;
   
   sum=0;
   printf "Car groups:\n";
   for (i in cargroupcounts) {
     sum += cargroupcounts[i];
     printf "%s: %d\n", i, cargroupcounts[i];
   }
   
   diff=int(racercount)-int(sum);
   if (diff==0)
      print "Car group check is OK";
   else
     printf "%d cargroups missed\n", diff;

   sum=0;
   printf "Car classes:\n";
   for (i in carclasscounts) {
     sum += carclasscounts[i];
     printf "%s: %d\n", i, carclasscounts[i];
   }
   
   diff=int(racercount)-int(sum);
   if (diff==0)
      print "Car class check is OK";
   else
     printf "%d carclass missed\n", diff;
   
   printf "Asn:";
   for (i in asncounts) 
     printf "%s: %d\n", i, asncounts[i];
}

