create table nation(id int not null auto_increment primary key,
	code varchar(10), name varchar(30) 
);

create table cargroup(id int not null auto_increment primary key,
	code varchar(10), name varchar(30) 
);

create table carclass(id int not null auto_increment primary key,
	group_id int, 
	code varchar(10), name varchar(30), 
	foreign key (group_id references cargroup(id) on delete cascade) 
);

create table participant(id int not null auto_increment primary key,
	name varchar(50) 
);

create table racer(id int not null auto_increment primary key,
	startnum int, 
	pilot varchar(30), 
	copilot varchar(30), 
	car varchar(30), 
	asn varchar(10), 
	group_id int, 
	carclass_id, int
	foreign key (part_id references participant(id) on delete cascade), 
	foreign key (carclass_id references carclass(id) on delete cascade)
 );

insert into nation (code, name) values ('H', 'Magyarország');
insert into nation (code, name) values ('HR', 'Horvátország');
insert into nation (code, name) values ('A', 'Ausztria');
insert into nation (code, name) values ('SK', 'Szlovákia');
insert into nation (code, name) values ('RO', 'Románia');
insert into nation (code, name) values ('SLO', 'Szlovénia');
insert into cargroup (code, name) values ('A','A csoport');
insert into cargroup (code, name) values ('N','N csoport');
insert into cargroup (code, name) values ('H','H csoport');
insert into cargroup (code, name) values ('WRC','WRC csoport');
insert into cargroup (code, name) values ('IRC','IRC csoport');
insert into cargroup (code, name, group_id) 
select 'N1', 'N1 géposztály', select cargroup.id from cargroup where 
cargroup.name='N';
insert into cargroup (code, name, group_id) 
select 'N2', 'N2 géposztály', select cargroup.id from cargroup where 
cargroup.name='N';
insert into cargroup (code, name, group_id) 
select 'N3', 'N3 géposztály', select cargroup.id from cargroup where 
cargroup.name='N';
insert into cargroup (code, name, group_id) 
select 'N4', 'N4 géposztály', select cargroup.id from cargroup where 
cargroup.name='N';
insert into cargroup (code, name, group_id) 
select 'A5', 'A5 géposztály', select cargroup.id from cargroup where 
cargroup.name='A';
insert into cargroup (code, name, group_id) 
select 'A6', 'A6 géposztály', select cargroup.id from cargroup where 
cargroup.name='A';
insert into cargroup (code, name, group_id) 
select 'A7', 'A7 géposztály', select cargroup.id from cargroup where 
cargroup.name='A';
insert into cargroup (code, name, group_id) 
select 'A8', 'A8 géposztály', select cargroup.id from cargroup where 
cargroup.name='A';
insert into cargroup (code, name, group_id) 
select 'H9', 'H9 géposztály', select cargroup.id from cargroup where 
cargroup.name='H';
insert into cargroup (code, name, group_id) 
select 'H10', 'H10 géposztály', select cargroup.id from cargroup where 
cargroup.name='H';

insert into participant(name) values ('BOMBA! Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 1, 'Ifj T�th J�nos', 'T�th Imre', 'Peugeot 206 WRC', 'FIA', 
participant.id, carclass.id 
from participant, carclass
where participant.name='BOMBA! Rallye Team' and carclass.code='8';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 2, 'Nenad Lon�ari�', 'Nenad Puzi�', 'Mitsubishi Evo VI', 'FIA', 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='8';

insert into participant(name) values ('OMV Best Racing Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 3, 'Benik Bal�zs', 'Somogyi P�l', 'Ford Focus WRC', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='OMV Best Racing Team' and carclass.code='8';

insert into participant(name) values ('Castrol-Buda�rs Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 4, 'Szab� Gergely', 'Ker�k Bal�zs', 'Skoda Octavia WRC', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Castrol-Buda�rs Motorsport' and carclass.code='8';

insert into participant(name) values ('Vmax Asi Rallye Club')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 5, 'ASI', null, 'Mitsubishi Lancer', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Vmax Asi Rallye Club' and carclass.code='8';

insert into participant(name) values ('PACOT-CAR Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 6, 'Ifj Angyalfi K�roly', 'Kaz�r Mikl�s', 'Subaru Impreza WRC', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='PACOT-CAR Motorsport' and carclass.code='8';

insert into participant(name) values ('Botka SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 7, 'Turi Tam�s', 'Ker�k Istv�n', 'Mitsubishi', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Botka SC' and carclass.code='8';

insert into participant(name) values ('Citro�n-Total Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 8, 'B�tor R�bert', 'T�th Vilmos', 'Citro�n Saxo Kit-Car', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Citro�n-Total Rallye Team' and carclass.code='6';

insert into participant(name) values ('Intermedia Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 9, 'Tagai Tam�s', 'Tagai R�bert', 'Subaru Impreza WRC', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Intermedia Motorsport' and carclass.code='8';

insert into participant(name) values ('Rawex SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 10, 'Oroszl�n L�szl�', 'Oroszl�n Tibor', 'Mitsubishi Lancer Evo VII.', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Rawex SC' and carclass.code='4';

insert into participant(name) values ('Bajai AVSE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 11, 'Vizin L�szl�', 'Doromby Zolt�n', 'Subaru LS 03', 'ASN', 
participant.id, carclass.id 
from participant, carclass
where participant.name='Bajai AVSE' and carclass.code='8';

insert into participant(name) values ('Citro�n-Total Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 12, 'Herczig Norbert', 'Herczig R�bert', 'Citro�n Sup 1600', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Citro�n-Total Rallye Team' and carclass.code='6';

insert into participant(name) values ('Stille ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 14, 'Szil�gyi J�nos', 'Domonkos Eszter', 'Mitsubishi Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Stille ASE' and carclass.code='8';

insert into participant(name) values ('CO-PARTNERS Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 16, 'Szeleczky Tam�s', 'Penderik L�szl�', 'Ford Escort WRC', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='CO-PARTNERS Motorsport' and carclass.code='8';

insert into participant(name) values ('R-MESTER Kft.')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 17, 'Kiss Ferenc', 'T�bori J�zsef', 'Subaru Impreza WRC', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='R-MESTER Kft.' and carclass.code='8';

insert into participant(name) values ('PACOT-CAR Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 18, 'Spitzm�ller Csaba', 'Hars�nyi Zsolt', 'Mitsubishi Lancer Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='PACOT-CAR Motorsport' and carclass.code='4';

insert into participant(name) values ('Vmax Asi Rallye Club')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 19, 'Schleider Ferenc', 'Horv�th Tibor', 'Mitsubishi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Vmax Asi Rallye Club' and carclass.code='4';

insert into participant(name) values ('Botka SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 20, 'Botka D�vid', 'Papp Gy�rgy', 'Mitsubishi Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Botka SC' and carclass.code='8';

insert into participant(name) values ('Gold Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 21, 'Garami Zolt�n', 'Pong� K�roly', 'Mitsubishi Lancer Evo VII.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Gold Rallye Team' and carclass.code='4';

insert into participant(name) values ('Enternet Rally Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 22, '�brah�m Zolt�n', 'T�th G�bor', 'Renault Megane Maxi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Enternet Rally Team' and carclass.code='7';

insert into participant(name) values ('TOP Race MSE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 23, 'Pandur Zsolt', 'T�th Zsolt', 'Mitsubishi Lancer Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='TOP Race MSE' and carclass.code='4';

insert into participant(name) values ('Pacotti Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 24, 'Tyavoda Csaba', 'Podina P�ter', 'Mitsubishi Lancer Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Pacotti Motorsport' and carclass.code='4';

insert into participant(name) values ('Kelenf�ldi AMSE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 25, 'Hank� L�szl�', 'Feh�rv�ri M�rk', 'Mitsubishi Lancer Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Kelenf�ldi AMSE' and carclass.code='8';

insert into participant(name) values ('TT Racing ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 26, 'T�rnok Tam�s', 'Herr M�ty�s', 'Renault Megane Maxi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='TT Racing ASE' and carclass.code='7';

insert into participant(name) values ('Prevent 2001 AMSC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 27, 'Vas Zolt�n', 'Dork� Szabolcs', 'Nissan Almera', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Prevent 2001 AMSC' and carclass.code='7';

insert into participant(name) values ('Hirt Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 28, 'Kr�zser Menyh�rt', 'Buchholz J�nos', 'Honda Civic Type-R', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Hirt Motorsport' and carclass.code='3';

insert into participant(name) values ('Lajtai Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 29, 'Lajtai Zsolt', 'G�czi Attila', 'Ford Escort RS Cosw.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Lajtai Rallye Team' and carclass.code='4';

insert into participant(name) values ('FF Rally SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 30, 'Ferencz Kriszti�n', 'Ferencz Ram�n', 'Fiat Punto S 1600', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='FF Rally SE' and carclass.code='6';

insert into participant(name) values ('MP Contact Alba Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 31, 'Gr�zly Attila', 'T�th Tam�s', 'Mitsubishi Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='MP Contact Alba Rallye Team' and carclass.code='8';

insert into participant(name) values ('AK Delta')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 32, 'Jaki Franja', 'Damir Bruner', 'Peugeot 306 S16', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AK Delta' and carclass.code='7';

insert into participant(name) values ('ROAD STAR MSE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 33, 'P�nzes Zsolt', 'Zach�r J�nos', 'Mitsubishi Lancer Evo III.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='ROAD STAR MSE' and carclass.code='8';

insert into participant(name) values ('MP Contact Alba Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 34, 'Dara G�bor', 'Benk? Andr�s', 'Opel Astra', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='MP Contact Alba Rallye Team' and carclass.code='7';

insert into participant(name) values ('Kelenf�ldi AMSE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 35, 'Markovics Gy�rgy', 'Szab� G�bor', 'Ford Escort Maxi Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Kelenf�ldi AMSE' and carclass.code='7';

insert into participant(name) values ('LAROCO MSC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 36, 'Dicsa Tibor', 'Kakuszi Csaba', 'VW Golf Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='LAROCO MSC' and carclass.code='7';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 37, 'Goran Franceti�', 'Sa?a Bittermann', 'Mazda 323 GTR', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='7';

insert into participant(name) values ('AK Delta')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 38, 'Dra?en Pigl', '?eljko Sutlovi�', 'Opel Astra', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AK Delta' and carclass.code='7';

insert into participant(name) values ('Pandant Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 39, 'Tisza Andor', 'Bagam�ri L�szl�', 'Mitsubishi Lancer Evo VII.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Pandant Rallye Team' and carclass.code='4';

insert into participant(name) values ('P�sz-Ger TITI ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 40, 'Gerencs�r Tibor', 'Menthy Zsolt', 'Mitsubishi Lancer Evo VII.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='P�sz-Ger TITI ASE' and carclass.code='4';

insert into participant(name) values ('Botka SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 41, 'Botka Kriszti�n', 'Mihalik P�ter', 'Mitsubishi Lancer Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Botka SC' and carclass.code='4';

insert into participant(name) values ('MP Contact Alba Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 42, 'F�l�p Rajmund', null, 'Mitsubishi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='MP Contact Alba Rallye Team' and carclass.code='4';

insert into participant(name) values ('Team Hungary Kft.')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 43, 'Dornai Tibor', 'Bunkoczi L�szl�', 'Mitsubishi Lancer Evo IV.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Team Hungary Kft.' and carclass.code='8';

insert into participant(name) values ('B.I.P. Aut�Sport Egyes�let')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 44, 'Moln�r J�zsef', 'Petr�czi R�ka', 'Mitsubishi Lancer Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='B.I.P. Aut�Sport Egyes�let' and carclass.code='4';

insert into participant(name) values ('Subaru Motors Sport Hungary SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 45, 'Oll� S�ndor', 'Kom�romi B�la', 'Subaru Impreza STI Coupe', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Subaru Motors Sport Hungary SE' and carclass.code='4';

insert into participant(name) values ('PACOT-CAR Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 46, 'Katona Zsolt', 'Tak�cs Attila', 'Subaru Impreza', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='PACOT-CAR Motorsport' and carclass.code='4';

insert into participant(name) values ('R-MESTER Kft.')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 47, 'Keszler M�ty�s', 'T�th Csaba', 'Audi S2 Coupe', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='R-MESTER Kft.' and carclass.code='8';

insert into participant(name) values ('Bajai AVSE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 48, 'Michna Ott�', 'Nagyv�radi R�bert', 'Opel Calibra 4x4', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Bajai AVSE' and carclass.code='8';

insert into participant(name) values ('Veszpr�mi Rally Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 49, 'Veszpr�mi Roland', 'R�nav�lgyi Endre', 'Peugeot 205 GTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Veszpr�mi Rally Team' and carclass.code='7';

insert into participant(name) values ('Gr�m�n Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 50, 'S�rosi Gy�rgy', '�rczk�vi Attila', 'Opel Astra Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Gr�m�n Rallye Team' and carclass.code='7';

insert into participant(name) values ('Ritmus ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 51, 'Ripka G�bor', 'Lenzs�r Orsolya', 'Opel Astra GSi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ritmus ASE' and carclass.code='7';

insert into participant(name) values ('P�csi Rally SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 52, 'Hering Gyula', 'Kerekes G�bor', 'Honda Int. Type-R', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='P�csi Rally SC' and carclass.code='7';

insert into participant(name) values ('Huberth Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 53, '?sz L�szl�', null, 'Subaru Impreza', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Huberth Motorsport' and carclass.code='8';

insert into participant(name) values ('Szajky Rallye Team SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 54, 'Trencs�nyi J�zsef', 'Trencs�nyi Tam�s', 'Mitsubishi Evo VI.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Szajky Rallye Team SE' and carclass.code='8';

insert into participant(name) values ('Aut�f�kusz Rallye Team SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 55, 'Cseri J�zsef', 'Nagy D�nes', 'Toyota Celica Turbo 4WD', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Aut�f�kusz Rallye Team SE' and carclass.code='4';

insert into participant(name) values ('R-Mester Kft.')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 56, 'Bal�zs J�zsef', 'Sz�nt� Szabolcs', 'VAZ 21074', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='R-Mester Kft.' and carclass.code='6';

insert into participant(name) values ('RMP Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 57, 'Krupp Zolt�n', 'Buna Zolt�n', 'Skoda Felicia Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='RMP Rallye Team' and carclass.code='6';

insert into participant(name) values ('Gy?ri ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 58, '�brah�m Imre', 'Fodor G�za', 'Skoda Felicia Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Gy?ri ASE' and carclass.code='6';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 59, 'Tomislav ?kali�', 'Zdravko ?kali�', 'Honda Civic 1.6 VTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='6';

insert into participant(name) values ('Berg ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 60, 'Janzer Gy�rgy', 'Hegyaljai Zsolt', 'Skoda Felicia Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Berg ASE' and carclass.code='6';

insert into participant(name) values ('BA-RO Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 61, 'Baracskai Zsolt', 'Szenner Zsolt', 'Lada 21083 i Maxi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='BA-RO Motorsport' and carclass.code='6';

insert into participant(name) values ('VR-Rally Aut�sport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 62, 'Kov�cs Zolt�n', 'Verba G�bor', 'Lada 21074', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='VR-Rally Aut�sport' and carclass.code='6';

insert into participant(name) values ('VR-Rally Aut�sport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 63, 'Ifj Ranga L�szl�', 'Papp Gy�rgy', 'Skoda Felicia', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='VR-Rally Aut�sport' and carclass.code='6';

insert into participant(name) values ('Pro-Feel ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 64, 'Hadik Andr�s', 'Kov�cs Istv�n', 'Opel Astra GSi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Pro-Feel ASE' and carclass.code='3';

insert into participant(name) values ('G�li�t Rally Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 65, 'Nagy Zolt�n', 'Beyer Vilmos', 'Peugeot 306', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='G�li�t Rally Team' and carclass.code='3';

insert into participant(name) values ('Ritmus ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 66, 'V�rkonyi Szabolcs', '�dor Szabolcs', 'Peugeot 306 S16', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ritmus ASE' and carclass.code='3';

insert into participant(name) values ('Reflex SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 67, 'Kov�cs Attila', 'T�th B�lint', 'Honda Civic Type-R', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Reflex SC' and carclass.code='3';

insert into participant(name) values ('PI-DO Racing Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 68, 'Hal�sz Zsolt', 'Nagy Attila', 'Renault Clio W.', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='PI-DO Racing Team' and carclass.code='3';

insert into participant(name) values ('Servico Rali Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 69, 'R�dli J�zsef', '?ri S�ndor', 'Peugeot 306', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Servico Rali Team' and carclass.code='3';

insert into participant(name) values ('Ritmus ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 70, 'Lantos Zsolt', 'Ol�h P�ter', 'Opel Astra', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ritmus ASE' and carclass.code='3';

insert into participant(name) values ('Reflex SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 71, 'Veriga G�bor', null, 'Renault Clio', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Reflex SC' and carclass.code='3';

insert into participant(name) values ('R-MESTER Kft.')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 72, 'Balogh J�nos', null, 'Honda Civic Type-R', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='R-MESTER Kft.' and carclass.code='3';

insert into participant(name) values ('TOM ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 73, 'Enrico Bigolin', null, 'Peugeot 205', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='TOM ASE' and carclass.code='3';

insert into participant(name) values ('Rad� Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 74, 'Hosty�nszky Attila', 'R�zsa Norbert', 'Honda Civic VTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Rad� Rallye Team' and carclass.code='2';

insert into participant(name) values ('BDM-ART Dukko-Lux Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 75, 'D�k�ny S�ndor', 'Farkas G�bor', 'Honda Civic VTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='BDM-ART Dukko-Lux Rallye Team' and carclass.code='2';

insert into participant(name) values ('Ventile Rallye SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 76, 'G�sp�r Istv�n', 'Tak�cs �gnes', 'Honda Civic VTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ventile Rallye SE' and carclass.code='2';

insert into participant(name) values ('BDM-ART Dukko-Lux Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 77, 'G�ldi P�ter', 'Kerekes J�zsef', 'Honda Civic VTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='BDM-ART Dukko-Lux Rallye Team' and carclass.code='2';

insert into participant(name) values ('BA-RO Motosport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 78, 'Boroznaki Tibor', 'Kiss Andr�s', 'Honda Civic VTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='BA-RO Motosport' and carclass.code='2';

insert into participant(name) values ('Aquincum Racing Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 79, 'Tak�cs G�bor', 'Ny�ki J�zsef', 'Honda Civic', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Aquincum Racing Team' and carclass.code='2';

insert into participant(name) values ('Ritmus ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 80, 'Hanz�ly P�ter', 'Ny�rf�s Julianna', 'Peugeot 106', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ritmus ASE' and carclass.code='2';

insert into participant(name) values ('Gy?ri ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 81, 'Mejkli Ern?', 'Ferenczi L�szl�', 'Skoda Felicia', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Gy?ri ASE' and carclass.code='5';

insert into participant(name) values ('Ventile Rallye SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 82, 'N�meth Szelep P�ter', 'B�res Unicum Attila', 'Skoda Felicia Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ventile Rallye SE' and carclass.code='5';

insert into participant(name) values ('Dynamic Rally Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 83, 'Szokolai Tibor', 'Szokolai Tam�s', 'Skoda Felicia', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Dynamic Rally Team' and carclass.code='5';

insert into participant(name) values ('Auto Corse SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 84, 'Kern N�ndor', 'D�r P�ter', 'Fiat Seicento Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Auto Corse SE' and carclass.code='5';

insert into participant(name) values ('AK Delta')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 85, 'Sini?a Crnojevi�', 'Martina Mareti�', 'Suzuki Swift 1.3 GTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AK Delta' and carclass.code='5';

insert into participant(name) values ('AK Delta')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 86, '?eljko Sopi�', 'Denis Pikula', 'Suzuki Swift 1.3 GTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AK Delta' and carclass.code='5';

insert into participant(name) values ('VE-GO SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 87, 'Palasics L�szl�', 'Palasics Attila', 'Skoda Felicia', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='VE-GO SE' and carclass.code='5';

insert into participant(name) values ('Gold Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 88, 'Feh�r Istv�n', 'Leirer Zsolt', 'VAZ 2105', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Gold Rallye Team' and carclass.code='5';

insert into participant(name) values ('Szajky Rallye Team SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 89, 'Papp R�bert', 'G�czy Tam�s', 'VAZ 2105', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Szajky Rallye Team SE' and carclass.code='5';

insert into participant(name) values ('V�ci Aut� SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 90, 'Suba Egon', 'Suba �kos', 'Skoda Felicia', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='V�ci Aut� SE' and carclass.code='5';

insert into participant(name) values ('Tisza�jv�rosi Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 91, 'Bodolai L�szl�', 'Szentk�ti Tam�s', 'Lada Samara', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Tisza�jv�rosi Rallye Team' and carclass.code='5';

insert into participant(name) values ('Rad� Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 92, 'Krupa Zolt�n', null, 'Suzuki Swift GTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Rad� Rallye Team' and carclass.code='5';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 93, 'Antun Vrbanovi�', 'Branimir Ku�ko', 'Skoda Felicia 1.3', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='5';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 94, 'Sini?a Milinkovi�', 'Denis Vidovi�', 'Ford KA Kit-Car', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='5';

insert into participant(name) values ('AK Delta')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 95, 'Marko Cavor', 'Nikola �urin', 'Zastava Yugo', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AK Delta' and carclass.code='5';

insert into participant(name) values ('Tit�nia ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 96, 'Dr T�th Istv�n', 'Bene Zolt�n', 'Nissan Micra 1.3 S', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Tit�nia ASE' and carclass.code='5';

insert into participant(name) values ('Tit�nia ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 97, 'Kocsomba K�roly', 'Martin�k Bal�zs', 'Skoda Felicia', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Tit�nia ASE' and carclass.code='5';

insert into participant(name) values ('AK Delta')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 98, 'Reno Turk', 'Mislav Batan', 'Suzuki Swift 1.3 GTi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AK Delta' and carclass.code='1';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 99, 'Tihomir Simon', 'David Mrzljak', 'Peugeot 106 rally 1.3', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='1';

insert into participant(name) values ('AMK Siget')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 100, 'Marko Lovak', 'Davor Matun', 'Citroen Xsara', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='AMK Siget' and carclass.code='1';

insert into participant(name) values ('Dynamic Rally Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 101, 'Peth? Istv�n', 'Lov�sz Tibor', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Dynamic Rally Team' and carclass.code='9';

insert into participant(name) values ('Matics Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 102, 'Matics Mih�ly', 'T�th G�bor', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Matics Rallye Team' and carclass.code='9';

insert into participant(name) values ('ZEST ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 103, 'Hodula Mih�ly', 'Szil�gyi Tam�s', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='ZEST ASE' and carclass.code='9';

insert into participant(name) values ('G�li�t Rally Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 104, 'Balatonyi �rp�d', 'Kirsching J�nos', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='G�li�t Rally Team' and carclass.code='9';

insert into participant(name) values ('DODO ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 105, 'Osv�th P�ter', 'Kov�cs Attila', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='DODO ASE' and carclass.code='9';

insert into participant(name) values ('Spindler ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 106, 'Spindler Jen?', 'Pap K�lm�n', 'Opel Kadett GSi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Spindler ASE' and carclass.code='10';

insert into participant(name) values ('Huberth Motorsport')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 107, 'Varga Cig�ny Zolt�n', 'Sand Ferenc', 'BMW M3', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Huberth Motorsport' and carclass.code='10';

insert into participant(name) values ('LAROCO MSC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 108, 'P�hi Csaba', null, 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='LAROCO MSC' and carclass.code='9';

insert into participant(name) values ('LAROCO MSC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 109, 'Dr Varga Attila', null, 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='LAROCO MSC' and carclass.code='9';

insert into participant(name) values ('TOM ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 110, 'Notheisz Attila', 'Kaluzsa Tam�s', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='TOM ASE' and carclass.code='9';

insert into participant(name) values ('Servico Rali Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 111, 'Trexler Zolt�n', 'Dr Barthos G�bor', 'VW Golf R-TOI', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Servico Rali Team' and carclass.code='10';

insert into participant(name) values ('MP Contact Alba Rallye Team')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 112, 'Kir�ly G�bor', 'Vil�gi P�ter', 'Lada VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='MP Contact Alba Rallye Team' and carclass.code='9';

insert into participant(name) values ('Reflex SC')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 113, 'Z�hony Zolt�n', 'Sz�p Csaba', 'VAZ VFTS', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Reflex SC' and carclass.code='9';

insert into participant(name) values ('Ritmus ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 114, 'Dudinszky Istv�n', 'K�nya S�ndor', 'Opel Kadett GSi', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Ritmus ASE' and carclass.code='10';

insert into participant(name) values ('Beach-B SE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 115, 'Szab� L�szl�', 'Holl� Tibor', 'Trabant 800 Mini Kit', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Beach-B SE' and carclass.code='9';

insert into participant(name) values ('Tit�nia ASE')
insert into racer (startnum, pilot, copilot, car, asn, part_id, carclass_id)
select 116, '�rd�gh Mikl�s', 'Trub� B�la', 'Ford Escort XR 3i', null, 
participant.id, carclass.id 
from participant, carclass
where participant.name='Tit�nia ASE' and carclass.code='9';
All racer counts:114
Nations:
H: 101
HR: 13
Nation check is OK
Car groups:
N: 36
A: 62
H: 16
Car group check is OK
Car classes:
1: 3
2: 7
3: 11
4: 15
5: 17
6: 11
7: 13
8: 21
9: 12
10: 4
Car class check is OK
Asn:: 103
ASN: 9
FIA: 2
