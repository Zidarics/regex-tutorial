#!/usr/bin/awk -f

BEGIN {
    FS=";";
    max=0;
    maxname="?"
}

int($19) > int(max) { 
    max=$19; 
    maxname=$11 
}

END { 
    printf "most expensive drug is:%s, price is %s Ft\n", maxname, max;
}
