#!/usr/bin/awk -f

function usage() {
   print "neither drug nor atc code!\n";
   print "usage: ./atc -v drug=<drug to find> <drug file>\n"; 
   exit;
}

BEGIN { FS=";"; 
	IGNORECASE=1;
	if (ARGC == 1) { 
	    usage();
            exit;
	}
	printf "echo \"drug:%s atc:%s\"\n", drug,atc;
	if (atc == "" && drug== "") {
	   usage();
           exit;
	}
	script=ARGV[1]
}


atc!=""&&$13~atc {
	printf "atc:%s name:%s price:%d\n", $13, $11, $19; 
}

drug!=""&&$11~drug {
	printf "name: %s -> atc:%s, price:%s\n", $11, $13,$19;  
	ac[$4] = $13;
}

END {
    for (i in ac) {
            code = ac[i];
            sub ("[0-9][0-9]$","",code);
            cmd = sprintf("./atc.awk -v atc=%s %s", code, script);
            system(cmd);
    }
}
