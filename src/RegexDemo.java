import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {

    private static final String QUICK_FOX = "The quick brown fox jumps over the lazy dog";
    private static final String VALID_HEX = "0xfe";
    private static final String INVALID_HEX = "0xfg";

    private static final String HEX_PATTERN = "0[xX][a-fA-F0-9]+";
    private static final String WORD_BOUNDARIES_NOT_GREEDY_PATTERN = "\\b.+?\\b";
    private static final String WORD_BOUNDARIES_GREEDY_PATTERN = "\\b.+\\b";
    //private static final String WORD_BOUNDARIES_NOT_GREEDY_CLASS_PATTERN = "\\b[^\\b]\\b"; not supported

    public boolean isMatch(String string, String regex) {
        boolean res = string.matches(regex);
        System.out.println(String.format("% matches to %s is:%s", string, regex, res ? "true" : "false"));
        return res;
    }

    public boolean isMatch(String string, Pattern pattern) {
        boolean res = pattern.matcher(string).matches();
        System.out.println(String.format("%s matches to %s is:%s", string, pattern.pattern(), res ? "true" : "false"));
        return res;
    }

    public List<String> matches(String string, String regex, boolean caseInsensitive) {
        System.out.println(String.format("============= >%s< matches >%s< with case%s =============", string,regex,
                                         caseInsensitive ? "insensitive":"sensitive"));
        Pattern pattern = Pattern.compile(regex, caseInsensitive ? Pattern.CASE_INSENSITIVE:0);
        Matcher matcher = pattern.matcher(string);
        List<String> result = new ArrayList<>();
        while(matcher.find()) {
            result.add(String.format("Startindex:%d,end index:%d, string:->%s<-", matcher.start(), matcher.end(), matcher.group()));
        }
        if (result.isEmpty())
            System.out.println("no matches");
        return result;
    }

    public List<String> matches(String string, String regex) {
        return matches(string,regex,false);
    }

    public void canonicalTest() {
        Pattern withCE = Pattern.compile("^a\u030A$",Pattern.CANON_EQ);
        Pattern withoutCE = Pattern.compile("^a\u030A$");
        String input = "\u00E5";
        isMatch(input, withCE);
        isMatch(input, withoutCE);
    }

    public void quantifierTest() {
        String empty = "";
        matches(empty, "a?").forEach(s->System.out.println(s));
        matches(empty, "a+").forEach(s->System.out.println(s));
        matches(empty, "a*").forEach(s->System.out.println(s));

        matches(empty, "a??").forEach(s->System.out.println(s));
        matches(empty, "a+?").forEach(s->System.out.println(s));
        matches(empty, "a*?").forEach(s->System.out.println(s));

        matches(empty, "a?+").forEach(s->System.out.println(s));
        matches(empty, "a++").forEach(s->System.out.println(s));
        matches(empty, "a*+").forEach(s->System.out.println(s));

    }

    public RegexDemo() {
    }

    public void test() {
        System.out.println(String.format("%s matches to %s is:%s", VALID_HEX, HEX_PATTERN, VALID_HEX.matches(HEX_PATTERN)));
        System.out.println(String.format("%s matches to %s is:%s", INVALID_HEX, HEX_PATTERN, INVALID_HEX.matches(HEX_PATTERN)));
        matches(QUICK_FOX, WORD_BOUNDARIES_NOT_GREEDY_PATTERN, true).forEach(s->System.out.println(s));
        matches(QUICK_FOX, WORD_BOUNDARIES_GREEDY_PATTERN, true).forEach(s->System.out.println(s));
        canonicalTest();
        quantifierTest();
    }

    public static void main(String args[]) {
        new RegexDemo().test();
    }
}
