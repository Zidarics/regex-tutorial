import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class RegexTest {

    /**
     * Create a regex to check the telephone number validity with the following format:
     *
     * country code: 2 or 3 number with + prefix like +36
     * optional delimiter space or tab (\t)
     * operator code: 2 number
     * number: 6 or 7 numbers
     */
    @Test
    public void telephoneNumbers() {
        String regex = "";
        assertTrue("+36 301234567".matches(regex));
        assertTrue("+36301234567".matches(regex));
        assertTrue("36301234567".matches(regex));
        assertTrue("36 301234567".matches(regex));
    }

    /**
     * Create a regex to check hungarian personal identity card number
     *
     * format: 6 numbers and two capital letters like 123456QW
     *
     */
    @Test
    public void checkIdentityCard() {
        String regex = "";
        assertTrue("123456QW".matches(regex));
        assertFalse("1234567W".matches(regex));
        assertFalse("12345678".matches(regex));
        assertFalse("1234567QW".matches(regex));
        assertFalse("?123456QW".matches(regex));
        assertFalse("1?23456QW".matches(regex));
    }

    /**
     * Create a regex checking decimal with a precision of 2
     */
    @Test
    public void decimal() {
        String regex = "";
        assertTrue("12345.67".matches(regex));
        assertFalse("12345.678".matches(regex));
        assertFalse("12345.67.8".matches(regex));
        assertFalse("12345.asd".matches(regex));
        assertFalse("asd.67".matches(regex));
        assertFalse(".678".matches(regex));
    }

    /**
     * Write a regex to validate a new username. Criteria for a valid username is as follows:
     * It should start with an English alphabet followed by alphanumeric characters.
     * No special characters allowed
     * Username length should be at least 3 and should not exceed 20.
     */
    @Test
    public void userName() {
        String regex="";
        assertTrue("ZaphodBeeblebrox".matches(regex));
        assertTrue("ZaphodBeeblebrox1".matches(regex));
        assertFalse("Zaphod Beeblebrox".matches(regex));
        assertFalse("".matches(regex));
        assertFalse("Z".matches(regex));
        assertFalse("Zaphod_Beeblebrox".matches(regex));
        assertFalse("ZaphodBeeblebroxZaphodBeeblebrox".matches(regex));
    }

    /**
     * For a given input string, how can you programmatically correct the number of spaces between words?
     * We should essentially replace double or triple spacing with a single space character.
     */

    @Test
    public void removeExtraSpaces() {
        String text = "  So   long   and   thanks   for   all   the   fish  ";
        String replaced = text.replaceAll("", "");
        replaced = replaced.replaceAll("", " ");
        assertTrue("So long and thanks for all the fish".equals(replaced));
    }

    /**
     * Extract all numbers from the given string
     */
    @Test
    public void extractNumbers() {
        int numbers[] = new int[] {7,1,42,2,43,3,44,3,45,174};

        String text = "the total of 7 items whiche are 1:42;2:43;3:44;4:45 is: 174";
        Pattern pattern = Pattern.compile("");
        Matcher matcher = pattern.matcher(text);
        Set<String> result = new HashSet<>();
        while(matcher.find()) {
            result.add(matcher.group());
        }

        assertEquals(result.size(), numbers.length);

        for (int i : numbers) {
            assertTrue(result.contains(Integer.toString(i)));
        }
    }

    /**
     * How many lines are there in this string?
     */
    @Test
    public void numberOfLines(){
        String text = "line Mac\rline linux\nline windows\r\n";
        assertEquals(3, text.split("").length);
    }

    /**
     * please change what and that in the string
     */
    @Test
    public void replaceWords(){
        String text = "that is what";
        assertEquals("what is that",text.replaceAll("", ""));
    }

    /**
     * Create string which is corresponds with the given regex
     */
    @Test
    public void stringCorrespondsWithRegex() {
        String regex = "\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|$)){4}\\b";
        assertTrue("".matches(regex));
        assertTrue("".matches(regex));
    }

    /**
     * Write a Regular expression to match line that doesn't contain a word theft
     */
    @Test
    public void notWord() {
        String regex = "";
        assertTrue("There was a crime incident".matches(regex));
        assertFalse("The incident involved a theft".matches(regex));
        assertFalse("The theft was a serious incident".matches(regex));
    }

}
















/*

telephoneNumbers: ^\+?\d{2,3}\s?\d{2}\d{6,7}$



















































checkIdentityCard: ^\d{6}[A-Z][A-Z]$



















































decimal: \d+(\.\d{1,2})?



















































userName: ^[A-Za-z][A-Za-z0-9]{2,19}



















































removeExtraSpaces: (^\s+|\s+$)  \s\s+



















































extractNumbers: \\d+



















































numberOfLines: (\r\n|\r|\n)



















































replaceWords: ^(.*?)(that)(.+?)(what)(.*?)$  $1$4$3$2$5



















































stringCorrespondsWithRegex: 193.6.55.17  8.8.8.8



















































notWord: (?!.*theft).*incident.*
 */